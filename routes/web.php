<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home
Route::view('/', 'welcome')->name('index');

// ABOUT
Route::view('/about', 'about')->name('about');

// Services
Route::prefix('services')->name('services.')->group(function() {
    // Services All
    Route::view('/best-digital-marketing-services-in-india','services')->name('all');
    Route::view('/best-ecommerce-website-development-company','services.ecommerce')->name('ecommerce');
    Route::view('/best-digital-marketing-company-in-india','services.digital-marketing')->name('digital-marketing');
    Route::view('/best-call-center-service-provider-in-india','services.call-center')->name('call-center');


});

// Contact US
Route::view('/contact-us', 'contact')->name('contact-us');
Route::post('send-mail','ContactController@sendMail')->name('contact.mail');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
