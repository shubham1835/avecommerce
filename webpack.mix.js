const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css');

mix.scripts([
    'resources/js/theme/vendor/jquery.min.js',
    'resources/js/theme/vendor/jquery.easing.min.js',
    'resources/js/theme/vendor/jquery.inview.min.js',
    'resources/js/theme/vendor/popper.min.js',
    'resources/js/theme/vendor/bootstrap.min.js',
    'resources/js/theme/vendor/ponyfill.min.js',
    'resources/js/theme/vendor/slider.min.js',
    'resources/js/theme/vendor/animation.min.js',
    'resources/js/theme/vendor/progress-radial.min.js',
    'resources/js/theme/vendor/bricklayer.min.js',
    'resources/js/theme/vendor/gallery.min.js',
    'resources/js/theme/vendor/shuffle.min.js',
    'resources/js/theme/main.js'
], 'public/js/all.js')
.styles([
    'resources/sass/theme/vendor/bootstrap.min.css',
    'resources/sass/theme/vendor/slider.min.css',
    'resources/sass/theme/main.css',
    'resources/sass/theme/vendor/icons.min.css',
    'resources/sass/theme/vendor/animation.min.css',
    'resources/sass/theme/vendor/gallery.min.css',
    'resources/sass/theme/default.css',
    'resources/sass/theme/theme-orange.css',
], 'public/css/all.css');
