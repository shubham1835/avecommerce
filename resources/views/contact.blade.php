@extends('layouts.web')

@section('title')
Contact Us - AV E-Commerce Solutions
@endsection

@section('styles')
<!-- ==============================================
    Theme Settings
    =============================================== -->
<style>
    :root {
        --header-bg-color: #111111;
        --nav-item-color: #f5f5f5;
        --hero-bg-color: #111111;

        --section-1-bg-color: #f5f5f5;
        --section-2-bg-color: #eeeeee;
        --section-3-bg-color: #f5f5f5;

        --footer-bg-color: #111111;
    }

    .map-container{
    overflow:hidden;
    padding-bottom:56.25%;
    position:relative;
    height:0;
    }
    .map-container iframe{
    left:0;
    top:0;
    height:100%;
    width:100%;
    position:absolute;
    }

</style>
@endsection

@section('content')

<!-- Hero -->
<section id="slider" class="hero p-0 odd featured">
    <div class="swiper-container no-slider slider-h-75">
        <div class="swiper-wrapper">

            <!-- Item 1 -->
            <div class="swiper-slide slide-center">

                <img src="{{asset('images/services/contact.jpg')}}" class="full-image mask">

                <div class="slide-content row text-center">
                    <div class="col-12 mx-auto inner">
                        <h1 data-aos="zoom-out-up" data-aos-delay="400" class="title effect-static-text">Contact Us</h1>
                        <nav data-aos="zoom-out-up" data-aos-delay="800" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Contacts -->
<section id="contacts" class="section-1 offers">
    <div class="container">
        <div class="row intro">
            <div class="col-12 col-md-9 align-self-center text-center text-md-left">
                <h2 class="featured">How Can We Help?</h2>
                <p>{{"Reach to us and let's have a cup of coffee and discuss your amazing requirement."}}</p>
            </div>
        </div>
        <div class="row justify-content-center text-center items">
            <div class="col-12 col-md-6 col-lg-4 item">
                <div class="card featured">
                    <i class="icon icon-phone"></i>
                    <h4>+91 84088 66664<br>+91 99235 33824<br>+91 80872 33970</h4>
                    <p>You can directly call or WhatsApp to this Number.</p>
                    <a href="tel:+918408866664"><i class="btn-icon icon-arrow-right-circle"></i></a>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 item">
                <div class="card">
                    <i class="icon icon-envelope"></i>
                    <h4>avecommercesolutions@gmail.com</h4>
                    <p>Write a Mail to us, we will get back to you ASAP.</p>
                    <a href="mailto:avecommercesolutions@gmail.com"><i class="btn-icon icon-arrow-right-circle"></i></a>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4 item">
                <div class="card featured">
                    <i class="icon icon-location-pin"></i>
                    <h4>Jafar Nagar, Nagpur - 440013</h4>
                    <p>You can reach to us at given address.</p>
                    <a href="#"><i class="btn-icon icon-arrow-right-circle"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Contact -->
<section id="contact" class="section-2 form">
    <div class="container">
        <h2>{{ "Locate on map" }}</h2>
        <div id="map-container-google-1" class="z-depth-1-half map-container" style="height: 500px">
        <iframe src="https://maps.google.com/maps?q=AV+E-Commerce+Solutions.LLP&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0"
            style="border:0" allowfullscreen></iframe>
        </div>
    </div>
</section>

@endsection
