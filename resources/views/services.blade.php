@extends('layouts.web')

@section('title')
    Best Digital Marketing services in India | Top class services
@endsection

@section('meta-description', 'We are known for Best Digital Marketing Services in India and also expanding our quality in ecommerce services like Amazon and Flipkart.')
@section('meta-keywords', 'marketing services, digital marketing services india, e-commerce service, e commerce service provider, call center service, calling services')

@section('styles')
<style>
    :root {
        --header-bg-color: #111111;
        --nav-item-color: #f5f5f5;
        --hero-bg-color: #111111;

        --section-1-bg-color: #eeeeee;
    
        --footer-bg-color: #111111;
    }

    .odd h2{
        color: #2f323a;
    }
</style>
@endsection

@section('content')
<!-- Hero -->
<section id="slider" class="hero p-0 odd featured">
    <div class="swiper-container no-slider slider-h-75">
        <div class="swiper-wrapper">
            <!-- Item 1 -->
            <div class="swiper-slide slide-center">

                <img data-aos="zoom-out-up" data-aos-delay="800" src="{{ asset('assets/images/bg-1.jpg') }}" class="full-image" alt="digital marketing services india">
                
                <div class="slide-content row text-center">
                    <div class="col-12 mx-auto inner">
                        <h1 data-aos="zoom-out-up" data-aos-delay="400" class="title effect-static-text">Digital Marketing Services India</h1>
                        <nav data-aos="zoom-out-up" data-aos-delay="800" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Services</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('partials.services')

@endsection
