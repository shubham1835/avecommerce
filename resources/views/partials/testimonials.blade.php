<!-- Team -->
<section id="team" class="section-6 odd carousel">
    <div class="overflow-holder">
        <div class="container">
            <div class="row text-center intro">
                <div class="col-12">
                    <h2 class="featured mt-0">Testimonials</h2>
                    <p class="text-max-800">This is what our client think about our services.</p>
                </div>
            </div>
            <div class="swiper-container mid-slider items">
                <div class="swiper-wrapper">
                    
                    <div class="swiper-slide slide-center text-center item">
                        <div class="row card">
                            <div class="col-12">
                                <h4>Venkat Rao</h4>
                                <p>Very Good Service By AV E-commerce Solutions.</p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="swiper-slide slide-center text-center item">
                        <div class="row card">
                            <div class="col-12">
                                <h4>Venkat Rao</h4>
                                <p>Very Good Service By AV E-commerce Solutions.</p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="swiper-slide slide-center text-center item">
                        <div class="row card">
                            <div class="col-12">
                                <h4>Venkat Rao</h4>
                                <p>Very Good Service By AV E-commerce Solutions.</p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="swiper-slide slide-center text-center item">
                        <div class="row card">
                            <div class="col-12">
                                <h4>Venkat Rao</h4>
                                <p>Very Good Service By AV E-commerce Solutions.</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>
</section>