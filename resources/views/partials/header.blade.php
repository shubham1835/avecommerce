<header id="header" class="odd">

    <!-- Navbar -->
    <nav data-aos="zoom-out" data-aos-delay="800" class="navbar navbar-expand">
        <div class="container header">

            <!-- Navbar Brand-->
            <a class="navbar-brand" href="{{route('index')}}">
                <img src="{{asset('images/logo.png')}}" alt="AV Ecommerce Solutions">
            </a>

            <!-- Nav holder -->
            <div class="ml-auto"></div>

            <!-- Navbar Items -->
            <ul class="navbar-nav items">
                <li class="nav-item {{Route::currentRouteName() == 'index' ? 'active' : ''}}">
                    <a href="{{route('index')}}" class="nav-link {{Route::currentRouteName() == 'index' ? 'active' : ''}}">Home</a>
                </li>
                <li class="nav-item {{Route::currentRouteName() == 'about' ? 'active' : ''}}">
                    <a href="{{route('about')}}" class="nav-link {{Route::currentRouteName() == 'about' ? 'active' : ''}}">About</a>
                </li>
                <li class="nav-item dropdown {{ Request::is('services/*') ? 'active' : '' }}">
                    <a href="{{ route('services.all') }}" class="nav-link {{ Request::is('services/*') ? 'active' : '' }}">Services <i class="icon-arrow-down"></i></a>
                    <ul class="dropdown-menu">
                        <li class="nav-item"><a href="{{ route('services.ecommerce') }}" class="nav-link {{ Request::is('services/*') ? 'active' : '' }}">E-commerce Solutions</a></li>
                        <li class="nav-item"><a href="{{ route('services.digital-marketing') }}" class="nav-link {{ Request::is('services/*') ? 'active' : '' }}">Digital Marketing</a></li>
                        <li class="nav-item"><a href="{{ route('services.call-center') }}" class="nav-link {{ Request::is('services/*') ? 'active' : '' }}">Call Center</a></li>
                    </ul>
                </li>
            </ul>

            <!-- Navbar Icons -->
            <ul class="navbar-nav icons">
                <li class="nav-item social">
                    <a href="https://www.facebook.com/avecommercesolutions" target="_blank" class="nav-link"><i class="icon-social-facebook"></i></a>
                </li>
                <li class="nav-item social">
                    <a href="https://www.linkedin.com/company/av-e-commerce-solutions" target="_blank" class="nav-link"><i class="icon-social-linkedin"></i></a>
                </li>
                <li class="nav-item social">
                    <a href="https://www.instagram.com/avecommercesolutions/" target="_blank" class="nav-link"><i class="icon-social-instagram"></i></a>
                </li>
            </ul>

            <!-- Navbar Toggle -->
            <ul class="navbar-nav toggle">
                <li class="nav-item">
                    <a href="#" class="nav-link" data-toggle="modal" data-target="#menu">
                        <i class="icon-menu m-0"></i>
                    </a>
                </li>
            </ul>

            <!-- Navbar Action -->
            <ul class="navbar-nav action">
                <li class="nav-item ml-3">
                    <a href="{{ route('contact-us') }}" class="btn ml-lg-auto dark-button"><i class="icon-phone"></i>CONTACT US</a>
                </li>
            </ul>
        </div>
    </nav>

</header>
