<!-- Team -->
<section id="team" class="section-6 odd carousel">
    <div class="overflow-holder">
        <div class="container">
            <div class="row text-center intro">
                <div class="col-12">
                    <h2 class="featured mt-0 text-black">Team of Experts</h2>
                    <p class="text-max-800">The Expert Behind the scene working effortlessly, Young Energatic and passoniate team for best in class service.</p>
                </div>
            </div>
            <div class="swiper-container mid-slider items">
                <div class="swiper-wrapper">
                    <div class="swiper-slide slide-center text-center item">
                        <div class="row card">
                            <div class="col-12">
                                <img src="{{asset('images/profile.png')}}" alt="Venkat Rao" class="person">
                                <h4>Venkat Rao</h4>
                                <p>CEO & MD</p>
                                {{--  <ul class="navbar-nav social share-list ml-auto">
                                    <li class="nav-item">
                                        <a href="#" class="nav-link"><i class="icon-social-instagram"></i></a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link"><i class="icon-social-facebook"></i></a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link"><i class="icon-social-linkedin"></i></a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link"><i class="icon-social-twitter"></i></a>
                                    </li>
                                </ul>  --}}
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide slide-center text-center item">
                        <div class="row card">
                            <div class="col-12">
                                <img src="{{asset('images/profile.png')}}" alt="Mary Evans" class="person">
                                <h4>Syed Hashir Husain</h4>
                                <p>BDM</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide slide-center text-center item">
                        <div class="row card">
                            <div class="col-12">
                                <img src="{{asset('images/profile.png')}}" alt="Sarah Lopez" class="person">
                                <h4>Sunny Singh</h4>
                                <p>CTO</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>
</section>