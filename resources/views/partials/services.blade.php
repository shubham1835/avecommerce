<!-- Services -->
<section id="services" class="section-4 odd offers featured">
    <div class="container">
        <div class="row intro">
            <div class="col-12 col-md-9 align-self-center text-center text-md-left">
                <h2 class="featured">Best Services</h2>
                <p>These are the area we hold expertise in, we believe in our great team effort and so would you. </p>
            </div>
            @if(!Request::is('services/*'))
            <div class="col-12 col-md-3 align-self-end">
                <a href="{{ route('services.all') }}" class="btn mx-auto mr-md-0 ml-md-auto primary-button"><i
                        class="icon-grid"></i>VIEW ALL</a>
            </div>
            @endif
        </div>
        <div class="row justify-content-center text-center items">
            <div class="col-12 col-md-6 col-lg-4 item">
                <a href="{{route('services.ecommerce')}}">
                    <div class="card featured">
                        <i class="icon icon-badge"></i>
                        <h4>E-Commerce Solutions</h4>
                        <p>We are passionate to provide the best ecommerce service that generates effective results.</p>
                        <i class="btn-icon icon-arrow-right-circle"></i>
                    </div>
                </a>
            </div>
            <div class="col-12 col-md-6 col-lg-4 item">
                <a href="{{route('services.digital-marketing')}}">
                    <div class="card">
                        <i class="icon icon-screen-tablet"></i>
                        <h4>Digital Marketing</h4>
                        <p>Our sensible and creative marketing approach can generate tremendous results for our client’s.</p>
                        <i class="btn-icon icon-arrow-right-circle"></i>
                    </div>
                </a>
            </div>
            <div class="col-12 col-md-6 col-lg-4 item">
                <a href="{{route('services.call-center')}}">
                    <div class="card featured">
                        <i class="icon icon-phone"></i>
                        <h4>Call Center</h4>
                        <p>We offer National and International call center service that runs 24 hours to provide endless support.</p>
                        <i class="btn-icon icon-arrow-right-circle"></i>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>