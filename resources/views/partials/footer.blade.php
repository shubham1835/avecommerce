<footer>

    <!-- Footer [links] -->
    <section id="footer" class="odd footer">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-3 p-3 text-center text-lg-left">
                    <div class="brand">
                        <a href="index.html" class="logo">
                           <img src="{{asset('images/logo.png')}}" alt="AV Ecommerce Solutions">
                        </a>
                    </div>
                    <p>Creative Ecommerce Solution Provider</p>
                </div>
                <div class="col-12 col-lg-3 p-3 text-center">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="{{ route('services.all') }}" class="nav-link">All Services</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('services.ecommerce') }}" class="nav-link">E-commerce Service</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('services.digital-marketing') }}" class="nav-link">Digital Marketing</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('services.call-center') }}" class="nav-link">Call Center</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-lg-3 p-3 text-center">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="{{ route('about') }}" class="nav-link">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('contact-us') }}" class="nav-link">Contact Us</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">Follow Us</a>
                        </li>
                        <ul class="navbar-nav social share-list mt-0" style="margin-left: 60px;">
                            
                            <li class="nav-item">
                                <a href="https://www.facebook.com/avecommercesolutions" target="_blank" class="nav-link"><i class="icon-social-facebook ml-0"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="https://www.linkedin.com/company/av-e-commerce-solutions" target="_blank" class="nav-link"><i class="icon-social-linkedin"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="https://www.instagram.com/avecommercesolutions/" target="_blank" class="nav-link"><i class="icon-social-instagram "></i></a>
                            </li>
                            {{--  <li class="nav-item">
                                <a href="#" class="nav-link"><i class="icon-social-twitter"></i></a>
                            </li>  --}}
                        </ul>
                    </ul>
                </div>
                <div class="col-12 col-lg-3 p-3 text-center">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="icon-phone mr-2"></i>
                                +91 84088 66664
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="icon-envelope mr-2"></i>
                                avecommercesolutions@gmail.com
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="icon-location-pin mr-2"></i>
                                Jafar Nagar, Nagpur - 440013
                            </a>
                        </li>
                        {{--  <li class="nav-item">
                            <a href="{{ route('contact-us') }}" class="mt-4 ml-auto mr-auto btn primary-button"><i class="icon-phone"></i>Contact Us</a>
                        </li>  --}}
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- Copyright -->
    <section id="copyright" class="p-3 odd copyright">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 p-3 text-center text-lg-left">
                    <p>Best E-commerce Service provider.</p>
                </div>
                <div class="col-12 col-md-6 p-3 text-center text-lg-right">
                    <p>© 2020 AV E-commerce Solutions.</p>
                </div>
            </div>
        </div>
    </section>

</footer>
