<section id="about" class="section-1 odd highlights image-right">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 align-self-center text-center text-md-left">
                <div class="row intro">
                    <div class="col-12 p-0">
                        <h2 class="featured alt">What we do</h2>
                        {{--  <p>AV E-Commerce Solutions is an E-Commerce solution provider Company of Nagpur, India. We offer overall E-Commerce services like Making E-Commerce Store, Managing Social Media Marketing, Email Marketing, Customer Support etc.</p>  --}}
                        <p>E-commerce has become a way of life for people all around the globe. Market is full of Ecommerce Solution Providers but with our top notch solutions we try to make every customer our first priority. Once used only by the tech-savvy to buy things not available in the neighborhood mall, it has now become omnipresent. People have their twists and specific needs when it comes to online shopping, and AV E-commerce Solutions is always there to help people get what they truly deserve.</p>

                        <p>AV E-commerce Solutions always looks forward to serving a large number of fortune clients, to formulate specifically designed E-commerce solutions. Our aim is to become the best ecommerce website design company in India and we are working hard to achieve goals.</p>

                        <p>Through proper planning, design, execution, and marketing, we provide high-quality E-commerce development services to our clients across the globe. AV E-commerce Solutions believes in providing professional, reasonable & exclusive e-commerce solutions and services to its clients. The team has been involved in designing, developing, managing & marketing e-commerce based web applications. With the ever-increasing demand for quick online services, AV E-commerce Solutions takes an advantage over its competitors by offering the best of services to its clients including complete Amazon account management, Amazon advertisement & marketing, listing optimization, inventory management, content management, and even E-commerce photoshoots, video shoots and digital marketing services in India.</p>

                        <p>Our extensive services also includes digital marketing services, social media marketing, email marketing and we strive to become the best digital marketing company in India. We also become the national and international call centre service providers.</p>

                        <p>{{"The entire proficient team of experts at AV with in-depth knowledge is what helps us in becoming a trusted partner for our clients. Our skilled team of experts is dedicated to assuring that every client receives the best results for their business, and makes sure that the client's needs are satisfied & reliable solutions are hastily delivered."}}</p>
                    </div>
                </div>
                <div class="row items">
                    <div class="col-12 p-0">
                        <div class="row item">
                            <div class="col-12 col-md-2 align-self-center">
                                <i class="icon icon-arrow-right"></i>
                            </div>
                            <div class="col-12 col-md-9 align-self-center">
                                <a href="{{ route('services.ecommerce') }}">
                                    <h4>Ecommerce Solutions</h4>
                                </a>
                                {{--  <p>We aim to provide research on client much needed online presence to generate maximum value and build the brand reputation online through E-Commerce channels.</p>  --}}
                            </div>
                        </div>
                        <div class="row item">
                            <div class="col-12 col-md-2 align-self-center">
                                <i class="icon icon-arrow-right"></i>
                            </div>
                            <div class="col-12 col-md-9 align-self-center">
                                <a href="{{ route('services.digital-marketing') }}">
                                    <h4>Digital Marketing</h4>
                                </a>
                                {{--  <p>We are a full-service web-based E-Commerce Solution agency that develops websites and online internet campaigns also. We offer digital strategy, planning & creativity, resulting in fully managed and highly successful online marketing campaigns.</p>  --}}
                            </div>
                        </div>
                        <div class="row item">
                            <div class="col-12 col-md-2 align-self-center">
                                <i class="icon icon-arrow-right"></i>
                            </div>
                            <div class="col-12 col-md-9 align-self-center">
                                <a href="{{ route('services.call-center') }}">
                                    <h4>Call Center</h4>
                                </a>
                                {{--  <p>A Good eCommerce call centre gives a small business a big business feel. 24-hour sales, order entry, payment processing, billing inquiries, and more.</p>  --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="gallery col-12 col-md-6">
                <a href="{{asset('assets/images/about-4.jpg')}}">
                    <img src="{{asset('assets/images/about-4.jpg')}}" class="fit-image" alt="Fit Image">
                </a>
            </div>
        </div>
    </div>
</section>