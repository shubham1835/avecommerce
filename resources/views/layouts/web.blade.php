<!DOCTYPE html>
<html lang="en">
<head>

        <!-- ==============================================
        Basic Page Needs
        =============================================== -->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->

        <title>@yield('title', 'Best Ecommerce Solution Provider | Get Ecommerce Sales quickly') </title>

        <meta name="description" content="@yield('meta-description')">
        <meta name="keywords" content="@yield('meta-keywords')">

        {{--  <meta name="description" content="E-commerce Solutions, Digital Marketing solutions">  --}}
        {{--  <meta name="subject" content="E-commerce Solutions">  --}}
        <meta name="author" content="Sentriqo IT Solutions">

        {{--  <meta name="google-site-verification" content="ZleUC8Wwb9kYbRwp01-vY-lTn5WWEa3UiHPXXMTJShs" />  --}}

        <!-- ==============================================
        Favicons
        =============================================== -->
        <link rel="shortcut icon" href="{{asset('images/logo.png')}}">
        <link rel="apple-touch-icon" href="{{asset('images/logo.png')}}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{asset('images/logo.png')}}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{asset('images/logo.png')}}">

        <!-- ==============================================
        Vendor Stylesheet
        =============================================== -->
        
        <link rel="stylesheet" href="{{asset('css/all.css')}}">

        {{--  Page Theme Color Setting  --}}
        @yield('styles')
       
        {{--  Google Analytics Code  --}}
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-176775147-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-176775147-1');
        </script>

        
    </head>

    <body>

        <!-- Header -->
        @include('partials.header')

        @yield('content')

        <!-- Footer -->
        @include('partials.footer')

        <!-- Modal [search] -->
        <div id="search" class="p-0 odd modal fade" role="dialog" aria-labelledby="search" aria-hidden="true">
            <div class="modal-dialog modal-dialog-slideout" role="document">
                <div class="modal-content full">
                    <div class="modal-header" data-dismiss="modal">
                        Search <i class="icon-close"></i>
                    </div>
                    <div class="modal-body">
                        <form class="row">
                            <div class="col-12 p-0 align-self-center">
                                <div class="row">
                                    <div class="col-12 p-0 pb-3">
                                        <h2>What are you looking for?</h2>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent diam lacus, dapibus sed imperdiet.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 p-0 input-group">
                                        <input type="text" class="form-control" placeholder="Enter Keywords">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 p-0 input-group align-self-center">
                                        <button class="btn primary-button"><i class="icon-magnifier"></i>SEARCH</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal [responsive menu] -->
        <div id="menu" class="p-0 odd modal fade" role="dialog" aria-labelledby="menu" aria-hidden="true">
            <div class="modal-dialog modal-dialog-slideout" role="document">
                <div class="modal-content full">
                    <div class="modal-header" data-dismiss="modal">
                        Menu <i class="icon-close"></i>
                    </div>
                    <div class="menu modal-body">
                        <div class="row w-100">
                            <div class="items p-0 col-12 text-center">
                                <!-- Append [navbar] -->
                            </div>
                            <div class="contacts p-0 col-12 text-center">
                                <!-- Append [navbar] -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Scroll [to top] -->
        <div id="scroll-to-top" class="odd scroll-to-top">
            <a href="#header" class="smooth-anchor">
                <i class="icon-arrow-up"></i>
            </a>
        </div>        
        
        <!-- ==============================================
        Google reCAPTCHA // Put your site key here
        =============================================== -->
        <script src="https://www.google.com/recaptcha/api.js?render=6Lf-NwEVAAAAAPo_wwOYxFW18D9_EKvwxJxeyUx7"></script>

        <!-- ==============================================
        Vendor Scripts
        =============================================== -->
        
        <script src="{{asset('js/all.js')}}"></script>
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5f4b53b71e7ade5df4452733/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
        </script>
        <!--End of Tawk.to Script-->
    </body>

</html>