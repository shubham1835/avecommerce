@extends('layouts.web')

@section('title')
    Get result with Best Digital Marketing Company in India | Creative Marketing
@endsection

@section('meta-description', 'Our creative marketing approach makes us the Best digital marketing company in India. SEO, PPC, social media marketing, email marketing etc.')
@section('meta-keywords', 'digital marketing agency near me, best digital marketing company in india, best seo services india, Social media marketing agency, ppc services india, branding agency in india, graphic design company in india')

@section('styles')
<style>
    :root {
        --header-bg-color: #111111;
        --nav-item-color: #f5f5f5;
        --hero-bg-color: #111111;

        --section-1-bg-color: #eeeeee;
    
        --footer-bg-color: #111111;
    }

    .odd h2{
        color: #2f323a;
    }
</style>
@endsection

@section('content')
<!-- Hero -->
<section id="slider" class="hero p-0 odd featured">
    <div class="swiper-container no-slider slider-h-75">
        <div class="swiper-wrapper">

            <!-- Item 1 -->
            <div class="swiper-slide slide-center">

                <img data-aos="zoom-out-up" data-aos-delay="800" src="{{asset('images/services/digitalmarketing1.jpg')}}" class="full-image" alt="best digital marketing company in india">
                
                <div class="slide-content row text-center">
                    <div class="col-12 mx-auto inner">
                        <h1 data-aos="zoom-out-up" data-aos-delay="400" class="title effect-static-text">Digital Marketing</h1>
                        <nav data-aos="zoom-out-up" data-aos-delay="800" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('services.all') }}">Services</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Digital Marketing</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="services" class="section-4 odd offers featured">
    <div class="container">
        <div class="row intro">
            <div class="col-12 col-md-9 align-self-center text-center text-md-left">
                <h2 class="featured">Digital Marketing Services</h2>
                <p>With our creative marketing approach we become the best digital marketing company in India. We provide a sensible marketing approach and user experiences that deliver the best results to clients. </p>
                <p>AV E-commerce Solutions focuses on growing your business strongly with digital marketing services. Whether you want to increase traffic to your website, conversions, or both, our team will help you design an internet marketing campaign that will help you reach your goals. We provide services like SEO, SMM, PPC, Brand Marketing, Content Creation, and Graphic Design that will definitely help you grow your business.</p>
            </div>
        </div>
        <div class="row justify-content-center text-center items">
            <div class="col-12 col-md-6 col-lg-4 item">
                <div class="card featured">
                    <i class="icon icon-badge"></i>
                    <h4>SEO</h4>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4 item">
                <div class="card">
                    <i class="icon icon-badge"></i>
                    <h4>SMM</h4>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4 item">
                <div class="card featured">
                    <i class="icon icon-badge"></i>
                    <h4>PPC</h4>
                </div>
            </div>
            
            <div class="col-12 col-md-6 col-lg-4 item">
                <div class="card featured">
                    <i class="icon icon-badge"></i>
                    <h4>Brand Marketing</h4>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4 item">
                <div class="card featured">
                    <i class="icon icon-badge"></i>
                    <h4>Content Creation</h4>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4 item">
                <div class="card featured">
                    <i class="icon icon-badge"></i>
                    <h4>Graphic Design</h4>
                </div>
            </div>

        </div>
    </div>
</section>

<!-- Single -->
<section id="blog" class="section-1 single">
    <div class="container">
        <div class="row content">
            <main class="col-12 col-lg-8 p-0">

                <!-- Text -->
                <div class="row">
                    <div class="col-12 align-self-center">
                        <h2 class="featured mt-0">Digital Marketing</h2>
                        <p>With our Digital Brand Commerce practice, you can increase your product/service reach beyond geographies. As part of our digital management consulting practice, we will help you take better decisions with data & analytics and drive product innovations at a faster pace. We will also help you with the adoption of the latest marketing technology that supports your business model.</p>

                        <p>Our team is now making efforts to provide Best seo services in India and around the globe. We are also becoming the best Social media marketing agency with our creative solutions.</p>

                        <p>To apply Branding, Digital + eCommerce and Design capabilities to enable Startups & Brands to meet Consumers needs and develop world-class skills & capabilities. Our strategy is focused on helping clients improve their operational performance, deliver their products & services more effectively & efficiently and grow their businesses in existing & new markets.</p>
                        <p>
                            <blockquote>We offer SEO, SMM, PPC, Google Ads, Facebook Ads, Social Media marketing, Brand Management, etc.</blockquote>
                        </p>

                        <div class="gallery">
                            <a href="{{asset('images/services/digitalmarketing.jpg')}}">
                                <img src="{{asset('images/services/digitalmarketing.jpg')}}" class="w-100" alt="best digital marketing company in india">
                            </a>
                        </div>

                        <h4>Result Oriented Marketing</h4>
                        <p>The impact of digitisation has been all pervasive. Yes, it started with sales and marketing processes; however, now its influence is visible in all the functions of an organisation. Every C-suite executive is feeling the impact of it in their roles.</p>
                    </div>
                </div>

            </main>
            <aside class="col-12 col-lg-4 pl-lg-5 p-0 float-right sidebar">
                
                         
                
                <!-- Features -->
                <div class="row item">
                    <div class="col-12 align-self-center">
                        <h4>Features</h4>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="#">Start an online business with little or no capital.</a>
                                <i class="icon-check"></i>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="#">Grow your business online with limited investments.</a>
                                <i class="icon-check"></i>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="#">Omnichannel Strategy to take your existing business Online.</a>
                                <i class="icon-check"></i>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="#">Omnichannel Strategy to take your existing business Online.</a>
                                <i class="icon-check"></i>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="#">Build traffic to web properties through SEO, SMO.</a>
                                <i class="icon-check"></i>
                            </li>
                        </ul>
                    </div>
                </div>


                
                <!-- Tags -->
                <div class="row item">
                    <div class="col-12 align-self-center">
                        <h4>Tags</h4>
                        <div class="ml--03">
                            <span class="badge tag active">E-Commerce</span>
                            <span class="badge tag active">Website</span>
                            <span class="badge tag active">Design</span>
                            <span class="badge tag active">Call Process</span>
                            <span class="badge tag active">Inventory</span>
                            <span class="badge tag active">Woocommerce</span>
                            <span class="badge tag active">Shopify</span>
                            <span class="badge tag active">Apps</span>
                            <span class="badge tag active">ERP</span>
                            <span class="badge tag active">Records</span>
                        </div>
                    </div>
                </div>

                <!-- Follow Us -->
                <div class="row item">
                    <div class="col-12 align-self-center">
                        <h4>Follow Us</h4>
                        <ul class="navbar-nav social share-list">
                            <li class="nav-item">
                                <a href="#" class="nav-link"><i class="icon-social-instagram ml-0"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link"><i class="icon-social-facebook"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link"><i class="icon-social-linkedin"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link"><i class="icon-social-twitter"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>  

            </aside>
        </div>
    </div>
</section>

@endsection
