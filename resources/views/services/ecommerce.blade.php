@extends('layouts.web')

@section('title')
    Best ecommerce website development company in India | AV Ecommerce Solutions
@endsection

@section('meta-description', 'We are the top notch ecommerce website development company with added services like ecommerce SEO, ecommerce photography, app design etc.')
@section('meta-keywords', 'e-commerce solutions company, ecommerce website development company, ecommerce with amazon, ecommerce seo, ecommerce photography, ecommerce app design')

@section('styles')
<style>
    :root {
        --header-bg-color: #111111;
        --nav-item-color: #f5f5f5;
        --hero-bg-color: #111111;

        --section-1-bg-color: #eeeeee;
    
        --footer-bg-color: #111111;
    }

    .odd h2{
        color: #2f323a;
    }
</style>
@endsection

@section('content')
<!-- Hero -->
<section id="slider" class="hero p-0 odd featured">
    <div class="swiper-container no-slider slider-h-75">
        <div class="swiper-wrapper">

            <!-- Item 1 -->
            <div class="swiper-slide slide-center">

                <img data-aos="zoom-out-up" data-aos-delay="800" src="{{asset('images/services/ecommerce1.jpg')}}" class="full-image" alt="ecommerce website development company">
                
                <div class="slide-content row text-center">
                    <div class="col-12 mx-auto inner">
                        <h1 data-aos="zoom-out-up" data-aos-delay="400" class="title effect-static-text">Ecommerce Solution Provider</h1>
                        <nav data-aos="zoom-out-up" data-aos-delay="800" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('services.all') }}">Services</a></li>
                                <li class="breadcrumb-item active" aria-current="page">E-Commerce Solutions</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="services" class="section-4 odd offers featured">
    <div class="container">
        <div class="row intro">
            <div class="col-12 col-md-9 align-self-center text-center text-md-left">
                <h2 class="featured">E-commerce Services</h2>
                <p>AV E-commerce Solutions runs extra mile to become the best ecommerce website development company. We ensure the smooth day-to-day operations of an e-commerce platform, including software integrations and merchant and retailer relationships. 
                Our team, here at AV, believes in providing the finest services to its clients and ensure that their stores are visually appealing, easy to navigate, and furnished with accurate, up-to-date content. Also, we provide the finest products from the top leading brands across the globe.</p>
            </div>
        </div>
        <div class="row justify-content-center text-center items">
            <div class="col-12 col-md-6 col-lg-4 item">
                <div class="card featured">
                    <i class="icon icon-badge"></i>
                    <h4>Complete Amazon account management</h4>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4 item">
                <div class="card">
                    <i class="icon icon-badge"></i>
                    <h4>Amazon Advertisement & Marketing</h4>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4 item">
                <div class="card featured">
                    <i class="icon icon-badge"></i>
                    <h4>Ecommerce photos & Videoshoot</h4>
                </div>
            </div>
            
            <div class="col-12 col-md-6 col-lg-4 item">
                <div class="card featured">
                    <i class="icon icon-badge"></i>
                    <h4>Complete Ecommerce design work</h4>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4 item">
                <div class="card featured">
                    <i class="icon icon-badge"></i>
                    <h4>Amazon Listing Optimization</h4>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4 item">
                <div class="card featured">
                    <i class="icon icon-badge"></i>
                    <h4>Shopify store setup and marketing</h4>
                </div>
            </div>

        </div>
    </div>
</section>

<!-- Single -->
<section id="blog" class="section-1 single">
    <div class="container">
        <div class="row content">
            <main class="col-12 col-lg-8 p-0">

                <!-- Text -->
                <div class="row">
                    <div class="col-12 align-self-center">
                        <h2 class="featured mt-0">E-Commerce Solutions</h2>
                        <p>{{"We are a full-service web-based E-Commerce Solution agency that develops websites and online internet campaigns also. We are the leading ecommerce solutions company that brings creative solutions for our client’s."}}</p>

                        <p>We offer digital strategy, planning & creativity, resulting in fully managed and highly successful online marketing campaigns. <br>
                        We give you a sensible Internet marketing approach providing superior brand and user experiences that deliver the result to clients.</p>
                        <p>
                            <blockquote>We offer overall E-Commerce services like Making E-Commerce Store, Managing Social Media Marketing, ecommerce with amazon, ecommerce photography, ecommerce SEO, ecommerce app development.</blockquote>
                        </p>
                        <div class="gallery">
                            <a href="{{asset('images/services/shop.jpg')}}">
                                <img src="{{asset('images/services/shop.jpg')}}" class="w-100" alt="ecommerce website development company">
                            </a>
                        </div>

                        <h4>Our Promise</h4>
                        <p>Implement Order Management Process: Enable real-time updates about the products. Advertise them on different platforms. Manage, monitor, and optimize the activity.</p>
                        <ul>
                            <li>eCommerce Website Design Services</li>
                            <li>eCommerce Website Development Services</li>
                            <li>eCommerce SEO Services for eCommerce Websites</li>
                            <li>Payment Gateways</li>
                            <li>eCommerce Fulfillment Services</li>
                        <ul>
                    </div>
                </div>

            </main>
            <aside class="col-12 col-lg-4 pl-lg-5 p-0 float-right sidebar">
                
                         
                
                <!-- Features -->
                <div class="row item">
                    <div class="col-12 align-self-center">
                        <h4>Features</h4>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="#">Offer an entirely branded experience including hosting, control panel, registration and organic web marketing functionalities.</a>
                                <i class="icon-check"></i>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="#">Spend time building your business and your vertical value proposition, not your SEO back-end functionalities.</a>
                                <i class="icon-check"></i>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="#">Bundle a front-end with an offering that your sales team will love selling.</a>
                                <i class="icon-check"></i>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="#">Add open source to your mix and relieve adoption fears and differentiate your sales pitch.</a>
                                <i class="icon-check"></i>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="#">we develop custom on-boarding client processes, extensions & modules specific to your industry and/or geography.</a>
                                <i class="icon-check"></i>
                            </li>
                        </ul>
                    </div>
                </div>


                
                <!-- Tags -->
                <div class="row item">
                    <div class="col-12 align-self-center">
                        <h4>Tags</h4>
                        <div class="ml--03">
                            <span class="badge tag active">SEO</span>
                            <span class="badge tag active">SMM</span>
                            <span class="badge tag active">PPC</span>
                            <span class="badge tag active">Google Ads</span>
                            <span class="badge tag active">Facebook Ads</span>
                            <span class="badge tag active">Brand</span>
                            <span class="badge tag active">Video Marketing</span>
                        </div>
                    </div>
                </div>

                <!-- Follow Us -->
                <div class="row item">
                    <div class="col-12 align-self-center">
                        <h4>Follow Us</h4>
                        <ul class="navbar-nav social share-list">
                            <li class="nav-item">
                                <a href="#" class="nav-link"><i class="icon-social-instagram ml-0"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link"><i class="icon-social-facebook"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link"><i class="icon-social-linkedin"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link"><i class="icon-social-twitter"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>  

            </aside>
        </div>
    </div>
</section>

@endsection
