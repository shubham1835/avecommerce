@extends('layouts.web')

@section('title')
    Best BPO Call Center Provider In India
@endsection

@section('styles')
<style>
    :root {
        --header-bg-color: #111111;
        --nav-item-color: #f5f5f5;
        --hero-bg-color: #111111;

        --section-1-bg-color: #eeeeee;
    
        --footer-bg-color: #111111;
    }

    .odd h2{
        color: #2f323a;
    }
</style>
@endsection

@section('content')
<!-- Hero -->
<section id="slider" class="hero p-0 odd featured">
    <div class="swiper-container no-slider slider-h-75">
        <div class="swiper-wrapper">

            <!-- Item 1 -->
            <div class="swiper-slide slide-center">

                <img data-aos="zoom-out-up" data-aos-delay="800" src="{{asset('images/services/call1.jpg')}}" class="full-image">
                
                <div class="slide-content row text-center">
                    <div class="col-12 mx-auto inner">
                        <h1 data-aos="zoom-out-up" data-aos-delay="400" class="title effect-static-text">BPO Call Center</h1>
                        <nav data-aos="zoom-out-up" data-aos-delay="800" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('services.all') }}">Services</a></li>
                                <li class="breadcrumb-item active" aria-current="page">BPO Call Center</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Single -->
<section id="blog" class="section-1 single">
    <div class="container">
        <div class="row content">
            <main class="col-12 col-lg-8 p-0">

                <!-- Text -->
                <div class="row">
                    <div class="col-12 align-self-center">
                        <h2 class="featured mt-0">BPO Call Center</h2>
                        <p>AV E-commerce Solutions works 24x7 to provide services to its clients. By putting enormous effort and dedication, our team provides 24-hour sales, order entries, payment processing, billing inquiries, and more.</p>
                        <p>By adopting a research-led approach, AV Ecommerce Silutions equips organizations with powerful insights that can transform the way they perceive business challenges. Our teams consists of domain-specific experts who can help companies develop innovative strategies in order to compete and win in the industry segment they operate.</p>
                        <p>
                            <blockquote>we do support Brands to track their Customer Engagement through our extensive platform. Companies can monitor their Net Promoter Score [NPS], Customer Satisfaction [CSAT] levels as well the Customer Engagement through Email, Chat, and Social Media channels. Syrow BrandLabs provide tools to track and measure the metrics that are more important for an Organisation to deliver incredible Customer Service in a fraction of the time.</blockquote>
                        </p>
                        <div class="gallery">
                            <a href="{{asset('images/services/call.jpg')}}">
                                <img src="{{asset('images/services/call.jpg')}}" class="w-100">
                            </a>
                        </div>

                        <h4>Our Promise</h4>
                        <p>Implement Order Management Process: Enable real-time updates about the products. Advertise them on different platforms. Manage, monitor, and optimize the activity.</p>
                        <ul>
                            <li>eCommerce Website Design Services</li>
                            <li>eCommerce Website Development Services</li>
                            <li>eCommerce SEO Services for eCommerce Websites</li>
                            <li>Payment Gateways</li>
                            <li>eCommerce Fulfillment Services</li>
                        <ul>

                        <h4>
                            Currently, we are working on the Australian process.
                            In a case on any query please feel free to contact us.
                        </h4>
                    </div>
                </div>

            </main>
            <aside class="col-12 col-lg-4 pl-lg-5 p-0 float-right sidebar">
                
                         
                
                <!-- Features -->
                <div class="row item">
                    <div class="col-12 align-self-center">
                        <h4>Features</h4>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="#">Experienced Workforce: Our team has the right knowledge and skill to handle your Customer queries.</a>
                                <i class="icon-check"></i>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="#">Professional Call Handling: Our team is trained with the right behavior and etiquette to provide a great Customer eXperience.</a>
                                <i class="icon-check"></i>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="#">Quick Resolution of Escalations: We provide resolution to escalations with short turnaround time adhering to the Client SLA’s.</a>
                                <i class="icon-check"></i>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="#">Timely Updates for Decision Making: We help you to get timely updates for decision making that significantly supports business.</a>
                                <i class="icon-check"></i>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="#">Improved Customer Satisfaction: We enhance Customer eXperience “inside-out” by being the human face of Business.</a>
                                <i class="icon-check"></i>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <a href="#">Significant Cost Savings: We account to significant cost savings by providing effective solution at affordable price.</a>
                                <i class="icon-check"></i>
                            </li>
                        </ul>
                    </div>
                </div>


                
                <!-- Tags -->
                <div class="row item">
                    <div class="col-12 align-self-center">
                        <h4>Tags</h4>
                        <div class="ml--03">
                            <span class="badge tag active">Customer Service</span>
                            <span class="badge tag active">24 X 7 Inbound Call</span>
                            <span class="badge tag active">Semi Voice</span>
                            <span class="badge tag active">24 X 7 Email, Chat</span>
                            <span class="badge tag active">Tele Calling Service</span>
                            <span class="badge tag active">Outbound Calls</span>
                        </div>
                    </div>
                </div>

                <!-- Follow Us -->
                <div class="row item">
                    <div class="col-12 align-self-center">
                        <h4>Follow Us</h4>
                        <ul class="navbar-nav social share-list">
                            <li class="nav-item">
                                <a href="#" class="nav-link"><i class="icon-social-instagram ml-0"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link"><i class="icon-social-facebook"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link"><i class="icon-social-linkedin"></i></a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link"><i class="icon-social-twitter"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>  

            </aside>
        </div>
    </div>
</section>

@endsection
