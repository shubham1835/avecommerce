@extends('layouts.web')

@section('title')
    About AV E-Commerce Solutions
@endsection

@section('styles')
<!-- ==============================================
    Theme Settings
    =============================================== -->
<style>
        :root {               
            --header-bg-color: #040402;
            --nav-item-color: #f5f5f5;
            --hero-bg-color: #040402;

            --section-1-bg-color: #111111;
            --section-2-bg-color: #191919;
            --section-3-bg-color: #111111;
            --section-4-bg-color: #040402;
            --section-5-bg-color: #111111;
            --section-6-bg-color: #040402;
            --section-7-bg-color: #191919;
            --section-8-bg-color: #111111;
            --section-9-bg-color: #191919;
            --section-10-bg-color: #040402;
        
            --footer-bg-color: #111111;
        }
    </style>
@endsection

@section('content')

<!-- Hero -->
<section id="slider" class="hero p-0 odd featured">
    <div class="swiper-container no-slider slider-h-75">
        <div class="swiper-wrapper">

            <!-- Item 1 -->
            <div class="swiper-slide slide-center">

                <img src="{{asset('assets/images/bg-4.jpg')}}" class="full-image mask">

                <div class="slide-content row text-center">
                    <div class="col-12 mx-auto inner">
                        <h1 data-aos="zoom-out-up" data-aos-delay="400" class="title effect-static-text">ABOUT US</h1>
                        <nav data-aos="zoom-out-up" data-aos-delay="800" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('index')}}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">About</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Single -->
<section id="single" class="section-1 single">
    <div class="container">
        <div class="row">
            <main class="col-12 col-lg-8 p-0">
                <div class="row">
                    <div class="col-12 align-self-center">
                        <h2 class="featured mt-0">About Us</h2>
                        <p>AV E-Commerce Solutions is an E-Commerce solution provider Company of Nagpur, India. We offer overall E-Commerce services like Making E-Commerce Store, Managing Social Media Marketing, Email Marketing, Customer Support etc.</p>
                        <p>We aim to provide research on client much needed online presence to generate maximum value and build the brand reputation online through E-Commerce channels. We are passionate about E-Commerce which gives effective business results for you.</p>
                        <p>
                            <blockquote>We are a full-service web-based E-Commerce Solution agency that develops websites and online internet campaigns also. We offer digital strategy, planning & creativity, resulting in fully managed and highly successful online marketing campaigns.</blockquote>
                        </p>
                        <p>We give you a sensible Internet marketing approach providing superior brand and user experiences that deliver the result to clients.</p>
                        <p>A Good eCommerce call centre gives a small business a big business feel.
                            We provide 24-hour sales, order entry, payment processing, billing inquiries, and more.</p>
                        <ul>
                            <li>E-Commerce Solutions</li>
                            <li>Digital Marketing</li>
                            <li>Call Center</li>
                        <ul>
                    </div>
                </div>
            </main>
            <aside class="col-12 col-lg-4 pl-lg-5 p-0 float-right sidebar">
                <div class="row">
                    <div class="col-12 align-self-center text-left">
                        <h4>Our Key Heighlights</h4>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex justify-content-center align-items-center">
                                <a href="#"><i class="icon icon-arrow-right"></i></a>
                                Best E-Commerce Solution Provider.
                            </li>
                            <li class="list-group-item d-flex justify-content-center align-items-center">
                                <a href="#l"><i class="icon icon-arrow-right"></i></a>
                                Best Digital Marketing Solution.
                            </li>
                            <li class="list-group-item d-flex justify-content-center align-items-center">
                                <a href="#"><i class="icon icon-arrow-right"></i></a>
                                Best Call Center Facility for E-Commerce.
                            </li>
                            <li class="list-group-item d-flex justify-content-center align-items-center">
                                <a href="#"><i class="icon icon-arrow-right"></i></a>
                                Expert Team member for each expertise domain.
                            </li>
                            <li class="list-group-item d-flex justify-content-center align-items-center">
                                <a href="#"><i class="icon icon-arrow-right"></i></a>
                                Best in class 24 X 7 Support for all customers.
                            </li>
                            <li class="list-group-item d-flex justify-content-center align-items-center">
                                <a href="#"><i class="icon icon-arrow-right"></i></a>
                                100% Customer Satisfaction in E-commerce and digital.
                            </li>
                        </ul>
                    </div>
                </div>
            </aside>
        </div>
    </div>
</section>

@include('partials.work')

@include('partials.team')

@endsection
