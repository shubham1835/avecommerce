@extends('layouts.web')

@section('title')
    Best Ecommerce Solution Provider | Get Ecommerce Sales quickly
@endsection

@section('meta-description', 'We are the best Ecommerce solution provider with an experienced approach. Our quality and transparency makes us win the game in the ecommerce industry.')
@section('meta-keywords', 'ecommerce solution provider, ecommerce website design company in india, digital marketing services in india, best digital marketing company in india, call center service providers')

@section('styles')
 <!-- ==============================================
    Theme Settings
    =============================================== -->
    <style>
        :root {               
            --header-bg-color: #040402;
            --nav-item-color: #f5f5f5;
            --hero-bg-color: #040402;

            --section-1-bg-color: #111111;
            --section-2-bg-color: #191919;
            --section-3-bg-color: #111111;
            --section-4-bg-color: #040402;
            --section-5-bg-color: #111111;
            --section-6-bg-color: #040402;
            --section-7-bg-color: #191919;
            --section-8-bg-color: #eeeeee;
            --section-9-bg-color: #191919;
            --section-10-bg-color: #040402;
        
            --footer-bg-color: #111111;
        }
    </style>
@endsection

@section('content')

<!-- Hero -->
<section id="slider" class="hero p-0 odd">
    <div class="swiper-container full-slider featured animation slider-h-100">
        <div class="swiper-wrapper">

            <!-- Item 1 -->
            <div class="swiper-slide slide-center">
                <img data-aos="zoom-out-up" data-aos-delay="800" src="{{asset('assets/images/hero-4.jpg')}}"
                    class="hero-image" alt="Best Ecommerce Solution Provider">
                <div class="slide-content row">
                    <div class="col-12 d-flex inner">
                        <div class="left align-self-center text-center text-md-left">
                            <h1 data-aos="zoom-out-up" data-aos-delay="400" class="title effect-static-text">Ecommerce Solution</h1>
                            <p data-aos="zoom-out-up" data-aos-delay="800" class="description">AV E-commerce solutions Help you a-z for your ecommerce store.</p>
                            <a href="#about" data-aos="zoom-out-up" data-aos-delay="1200"
                                class="smooth-anchor ml-auto mr-auto ml-md-0 mt-4 btn dark-button"><i
                                    class="icon-cup"></i>GET STARTED</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Item 2 -->
            <div class="swiper-slide slide-center">
                <img data-aos="zoom-out-up" data-aos-delay="800" src="{{asset('assets/images/hero-5.jpg')}}"
                    class="hero-image" alt="Best Ecommerce Solution Provider">
                <div class="slide-content row">
                    <div class="col-12 d-flex inner">
                        <div class="left align-self-center text-center text-md-left">
                            <h2 data-aos="zoom-out-up" data-aos-delay="400" class="title effect-static-text">A Touch of
                                Creativity.</h2>
                            <p data-aos="zoom-out-up" data-aos-delay="800" class="description">AV E-commerce Solutions help you with creative ideas to build a great ecommerce brand.</p>
                            <a href="#about" data-aos="zoom-out-up" data-aos-delay="1200"
                                class="smooth-anchor ml-auto mr-auto ml-md-0 mt-4 btn dark-button"><i
                                    class="icon-cup"></i>GET STARTED</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Item 3 -->
            <div class="swiper-slide slide-center">
                <img data-aos="zoom-out-up" data-aos-delay="800" src="{{asset('assets/images/hero-6.jpg')}}"
                    class="hero-image" alt="Best Ecommerce Solution Provider">
                <div class="slide-content row">
                    <div class="col-12 d-flex inner">
                        <div class="left align-self-center text-center text-md-left">
                            <h2 data-aos="zoom-out-up" data-aos-delay="400" class="title effect-static-text">For Digital
                                Marketing.</h2>
                            <p data-aos="zoom-out-up" data-aos-delay="800" class="description">AV E-commerce Solutions help you in SEO, SMM, Facebook Ads, Google Ads, Instagram Adds and many more.</p>
                            <a href="#about" data-aos="zoom-out-up" data-aos-delay="1200"
                                class="smooth-anchor ml-auto mr-auto ml-md-0 mt-4 btn dark-button"><i
                                    class="icon-cup"></i>GET STARTED</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="swiper-pagination"></div>
    </div>
</section>

<!-- About [image] -->
@include('partials.work')

<!-- Fun Facts -->
<section id="funfacts" class="section-3 odd counter funfacts featured">
    <div class="container">
        <div class="row text-center intro">
            <div class="col-12">
                <h2>Fun Facts</h2>
                <p class="text-max-800">Here are some fun facts about AV E-Commerce Solutions, these are just numbers we believe in 100% customer satisfaction, and exactly what we deliver.</p>
            </div>
        </div>
        <div data-aos-id="counter" data-aos="fade-up" data-aos-delay="200"
            class="row justify-content-center text-center items">
            <div class="col-12 col-md-6 col-lg-3 item">
                <div data-percent="50" class="radial">
                    <span></span>
                </div>
                <h4>Great Projects</h4>
            </div>
            <div class="col-12 col-md-6 col-lg-3 item">
                <div data-percent="10000" class="radial">
                    <span></span>
                </div>
                <h4>Orders</h4>
            </div>
            <div class="col-12 col-md-6 col-lg-3 item">
                <div data-percent="500" class="radial">
                    <span></span>
                </div>
                <h4>Digital Ads</h4>
            </div>
            <div class="col-12 col-md-6 col-lg-3 item">
                <div data-percent="100" class="radial">
                    <span></span>
                </div>
                <h4>Happy Customers</h4>
            </div>
        </div>
    </div>
</section>

{{--  Services  --}}
@include('partials.services')

{{--  Testimonials  --}}
{{--  @include('partials.testimonials')  --}}

@endsection
