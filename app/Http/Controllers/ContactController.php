<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Send Contact Mail TO Admin And Thank You Mail TO Client
     * @param Request $request
     * @return Response Success or Fail
     * @author Shani Singh 
     */
    public function sendMail(Request $request)
    {
        dd($request->all());
    }
}
